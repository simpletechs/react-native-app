module.exports = {
  "presets": [
    "flow",
    "react-native",
    "react-native-stage-0/decorator-support"
  ],
  "env": {
    "development": {
      "plugins": [
        "transform-react-jsx-source"
      ]
    },
    "production": {
      "plugins": [
        "transform-remove-console"
      ]
    }
  }
}
