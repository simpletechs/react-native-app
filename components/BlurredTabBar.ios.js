// @flow
import React from 'react'
import { StyleSheet } from 'react-native'
import { BlurView } from 'react-native-blur'
import { TabBarBottom } from 'react-navigation'

type Props = {
  blurContainerStyle: Object,
}
export class BlurredTabBar extends React.PureComponent<Props> {
  render() {
    const {
      blurContainerStyle,
      ...restProps
    } = this.props

    return (
      <BlurView blurType='xlight' blurAmount={100} style={blurContainerStyle}>
        <TabBarBottom {...restProps} />
      </BlurView>
    )
  }
}
