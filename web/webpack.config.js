// web/webpack.config.js

const path = require('path');
const webpack = require('webpack');
const fs = require('fs');
const _ = require('lodash');
const appDirectory = process.cwd();
const babelrc = require('../.babelrc')

console.log('using .babelrc.js file from', )

const babelOptions = _.merge({}, babelrc, {
  cacheDirectory: true,
  babelrc: true,
  // Babel configuration (or use .babelrc)
  // This aliases 'react-native' to 'react-native-web' and includes only
  // the modules needed by the app.
  // plugins: ['transform-react-jsx-source'],
  // The 'react-native' preset is recommended to match React Native's packager
  // presets: ['babel-preset-flow', 'babel-preset-react-native', 'babel-preset-react-native-stage-0/decorator-support']
})

console.log(babelOptions)
// This is needed for webpack to compile JavaScript.
// Many OSS React Native packages are not compiled to ES5 before being
// published. If you depend on uncompiled packages they may cause webpack build
// errors. To fix this webpack can be configured to compile to the necessary
// `node_module`.
const babelLoaderConfiguration = {
  test: /\.js$/,
  // Add every directory that needs to be compiled by Babel during the build.
  include: [
    path.resolve(appDirectory, 'index.web.js'),
    path.resolve(appDirectory, 'src'),
    path.resolve(appDirectory, 'node_modules/react-navigation'),
    path.resolve(appDirectory, 'node_modules/react-native-tab-view'),
    path.resolve(appDirectory, 'node_modules/react-native-safe-area-view'),
    path.resolve(appDirectory, 'node_modules/react-native-uncompiled')
  ],
  use: {
    loader: 'babel-loader',
    options: babelOptions
  }
};

// This is needed for webpack to import static images in JavaScript files.
const imageLoaderConfiguration = {
  test: /\.(gif|jpe?g|png|svg)$/,
  use: {
    loader: 'url-loader',
    options: {
      name: '[name].[ext]'
    }
  }
};

const vectorIconsConfiguration = {
  test: /\.ttf$/,
  loader: "url-loader", // or directly file-loader
  include: path.resolve(appDirectory, "node_modules/react-native-vector-icons"),
}

module.exports = {
  devServer: { // TODO: not in documentation
    contentBase: path.join(appDirectory, 'web/public'),
    historyApiFallback: true // compatible with react-router. 404 => index.html
  },
  // your web-specific entry file
  entry: path.resolve(appDirectory, 'index.web.js'),

  // configures where the build ends up
  output: {
    filename: 'bundle.web.js',
    path: path.resolve(appDirectory, 'dist')
  },

  // ...the rest of your config

  module: {
    rules: [
      babelLoaderConfiguration,
      imageLoaderConfiguration,
      vectorIconsConfiguration
    ]
  },

  plugins: [
    new webpack.SourceMapDevToolPlugin({

    }),
    // `process.env.NODE_ENV === 'production'` must be `true` for production
    // builds to eliminate development checks and reduce build size. You may
    // wish to include additional optimizations.
    new webpack.DefinePlugin({
      // 'document': '{createElement: function() {}}',
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
      __DEV__: process.env.NODE_ENV === 'production' || true
    })
  ],

  resolve: {
    // If you're working on a multi-platform React Native app, web-specific
    // module implementations should be written in files using the extension
    // `.web.js`.
    symlinks: false, // is is required in order to be able to symlink this package in development
    extensions: ['.web.js', '.js'],
    alias: { // TODO: not in documentation, this is the magic!
      'react-native': 'react-native-web',
      'victory-native': 'victory'
    }
  }
}
