# Setup new App

* use `react-native init <AppName>`
* (change `name` of the `package.json` as it does not support uppercasing!)
* run `git init`
* `rm .babelrc` as it will be symlinked
* `rm .flowconfig` as it will be symlinked
    - make sure that the version match with the current react-native version!
* `yarn add simpletechs-react-native-app`
* run `node ./node_modules/simpletechs-react-native-app/scripts/init.js` (TODO: could be triggered by an `install` npm script)
* install any peer dependencies you need
    - `react-navigation` and `react-native-blur` are required for `BlurredTabBar`
* run `react-native link` to install native components

This adds all project files as symlinks to your project.


# How it works / Considerations

* React Native Web is a parallel build step that starts at index.web.js and loads all components.
* It will prefer `.web.js` files
* React Native can be developed almost independently, but web might break
* If a component breaks on web, use a parallel implementation using `MyComponent.web.js` which fulfills the same interface as `MyComponent.js`
* Components just be as isolated as possible => easier to fix web bugs by only adapting a single component
* React-Navigation does not work for Web!
* you can use web specific CSS with `StyleSheet`, but it needs to be wrapped inside `Platform.select({ web: { myCssProp: 5 }})` otherwise it will crash native
