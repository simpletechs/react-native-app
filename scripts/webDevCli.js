#!/usr/bin/env node
const path = require('path')
const { spawn } = require('child_process')

const appDir = process.cwd()

const command = './node_modules/.bin/webpack-dev-server'
const child = spawn(path.join(appDir, command), [
  '-d',
  '--config',
  path.join(__dirname, '../web/webpack.config.js'),
  '--inline',
  '--hot',
  '--colors'
])

child.stdout.pipe(process.stdout)
child.stderr.pipe(process.stderr)

child.on('close', (code) => {
  console.log(`child process exited with code ${code}`)
})
