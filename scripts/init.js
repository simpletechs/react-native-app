#!/bin/node
const fs = require('fs')
const path = require('path')

// these files should be common between all projects
const files = [
    '.vscode',
    '.babelrc.js',
    '.flowconfig',
    '.editorconfig'
]

const filesToCopy = [ // these files need to be modified by the app
  // '.gitignore',
  'web/public/index.html'
]

const projectDir = process.cwd()
console.log('creating symlinks in work dir %s for files:', projectDir)

files.forEach(file => {
  const src = path.join(__dirname, '../', file)
  const target = path.join(projectDir, file)
  console.log('creating symlink for %s -> %s', src, target)
  try {
    fs.symlinkSync(src, target)
  } catch(e) {
    console.error('could not symlink', file, e.message)
  }
})

console.log('copying files')
filesToCopy.forEach(file => {
  const src = path.join(__dirname, '../', file)
  const target = path.join(projectDir, file)
  console.log('copying %s -> %s', src, target)
  fs.copyFileSync(src, target)
})

console.log('done')
