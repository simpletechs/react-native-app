
/**
 * This script allows that you locally link the simpletechs-react-native modules using "yarn link".
 * The problem is that the metro bundler for react native tries to resolve modules relative to the requiring module.
 * This means it will try to load "react" and "react-native" from the simpletechs-react-native-app folder instead of your project!
 *
 * This script will generate a workaround configuration for your project and start the metro bundler with it.
 * see:
 * https://github.com/facebook/metro/issues/1#issuecomment-346502388
 * https://gist.github.com/GingerBear/485f922a1e403739dc56d279925b216d
 * - check symlink in depencency and devDepency
 * - if found, generate rn-cli-config.js
 * - react-native start with rn-cli-config
 */

const fs = require('fs');
const path = require('path');
const exec = require('child_process').execSync;
const RN_CLI_CONFIG_NAME = `rn-cli-config-with-simpletechs-links.js`;
const APP_DIR = process.cwd();
const packageJson = require(path.join(APP_DIR, 'package.json'));
main();

function main() {
  const deps = Object.keys(
    Object.assign({}, packageJson.dependencies, packageJson.devDependencies)
  );

  const symlinkPathes = getSymlinkPathes(deps);
  generateRnCliConfig(symlinkPathes, RN_CLI_CONFIG_NAME);
  runBundlerWithConfig(RN_CLI_CONFIG_NAME);
}

function getSymlinkPathes(deps) {
  const depLinks = [];
  const depPathes = [];
  deps.forEach(dep => {
    const stat = fs.lstatSync(path.join(APP_DIR, 'node_modules', dep));
    if (stat.isSymbolicLink()) {
      depLinks.push(dep);
      depPathes.push(fs.realpathSync(path.join(APP_DIR, 'node_modules', dep)));
    }
  });

  console.log('Starting react native with symlink modules:');
  console.log(
    depLinks.map((link, i) => '   ' + link + ' -> ' + depPathes[i]).join('\n')
  );

  return depPathes;
}

function generateRnCliConfig(symlinkPathes, configName) {
  const fileBody = `
var path = require('path');
var blacklist;
try {
  blacklist = require('metro-bundler/src/blacklist');
} catch(e) {
  blacklist = require('metro/src/blacklist');
}

var config = {
  extraNodeModules: {
    'react-native': path.resolve(__dirname, 'node_modules/react-native')
  },
  getBlacklistRE() {
    return blacklist([
      ${[...symlinkPathes.map(
      path =>
        `/${path.replace(
          /\//g,
          '[/\\\\]'
        )}[/\\\\]node_modules[/\\\\]react-native[/\\\\].*/`
  ), /.*\/__fixtures__\/.*/].join(',\n')}
    ]);
  },
  getProjectRoots() {
    return [
      // Keep your project directory.
      path.resolve(__dirname),

      // Include your forked package as a new root.
      ${symlinkPathes.map(path => `path.resolve('${path}')`)}
    ];
  }
};
module.exports = config;
  `;

  fs.writeFileSync(configName, fileBody);
}

function runBundlerWithConfig(configName) {
  exec(
    `node node_modules/react-native/local-cli/cli.js start --reset-cache --config ../../../../${
    configName
    }`,
    { stdio: [0, 1, 2] }
  );
}
